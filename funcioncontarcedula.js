

/*mandamos como parámetro un número en este caso una constante*/
function contar_par_impar(cedula){
    /*establecemos dos constante para almanacenar cuantos números pares e impares existen*/
   let par =0;
    let impar=0;

    /*hacemos un ciclo for de tipo in, para cálcular por cada número
    cual es par o impar*/
    for(numero in cedula){
        /*condición par*/
        if (numero % 2 == 0){
                /*sumamos uno al contador*/
            par = par+ 1
        }

        /*condición contraria a par, es decir impar*/
        if(numero % 2 !== 0){
            impar=impar+1
        }

        
     
    }

    /*returnamos los dos contadores*/
    return par, impar;
}

const númerodecédula=[1,3,4,5,6,7,8,9,0] 
/*hacemos llamada a la función y le pasamos el parámetro de la cédula*/
resultado = contar_par_impar(númerodecédula);

