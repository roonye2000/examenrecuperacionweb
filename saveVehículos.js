/*GUARDANDO DATOS DE VEHÍCULOS CON MONGODB*/

const moongose = require('mongoose');
const VehículoSchema = require('./models');

/*conexión CON LA BASE DE DATOS MEDIANTE VARIABLES DE ENTORNO, 
ESTABLECER la variable MONGO_URI con la conexión de mongodb*/

const dbConnection = async ()=>{

    try{
        await mongoose.connect(process.env.MONGO_URI);
        console.log('Base de datos conectada')
    }
    catch(error){
        console.log(error);
        throw new Error('Error! base de datos no encontrada')
    }

}
/*llamamos a la función*/
dbConnection()

/*Creamos el esquema que se gaurdará en mongo db*/
const VehículoSchema = new mongoose.Schema(
    {   
        codigo: {type:Number},
        descripcion: {type:String},
        marca: {type:String},
        modelo: {type:String},
        año: {type:Date},

    }, 
)

/*función asíncrona para mandar los datos*/

async function saveVehículos(req,res) {
    

      const vehículo =  await VehículoSchema.create({
            codigo: 1,
            descripcion: 'Carro 4 cabinas',
            marca:' chevrolet',
            modelo:'Malibu',
            año: '2022'
        }) 

        /*Mostramos por consola los datos*/
        console.log('Dato guardado correctamente', vehículo ); 
     
}

/*Hacemos llamado a la función*/

saveVehículos();
